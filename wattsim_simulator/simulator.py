from random import random
from time import sleep
from urllib.parse import urljoin
import logging
import requests

logger = logging.getLogger(__name__)


class StationSimulator(object):

    def __init__(self,
                 url='http://localhost:8000/',
                 username=None,
                 password=None,
                 frequency=10):
        self.base_url = url
        self.username = username
        self.password = password
        self.frequency = frequency

        self.session = requests.Session()
        self.session.headers['Referrer'] = self.base_url

    def authenticate(self):
        url = urljoin(self.base_url, '/api/v1/user/')
        logger.debug('Authenticating to {!r} with username {!r}'.format(
            url,
            self.username,
        ))

        resp = self.session.post(url, json={
            'username': self.username,
            'password': self.password,
        })
        resp.raise_for_status()

        logger.debug('Setting X-CSRFToken to {!r}'.format(
            self.session.cookies['csrftoken']))
        self.session.headers['X-CSRFToken'] = self.session.cookies['csrftoken']

    def build_payload(self):
        payload = {
            name: random() * 20
            for name in ('solar', 'hydro', 'wind')
        }
        logger.debug('Built measurement payload: {!r}'.format(payload))
        return payload

    def send(self):
        url = urljoin(self.base_url, '/api/v1/measurements/')
        logger.debug('Uploading measurement to {!r}'.format(url))
        resp = self.session.post(url, json=self.build_payload())
        resp.raise_for_status()
        logger.debug('New measurement ID: {!r}'.format(resp.json()['id']))

    def run(self):
        logger.info('Authenticating…')
        self.authenticate()
        logger.info('Starting measurement uploads every {:.1f} seconds'.format(
            self.frequency))
        while True:
            self.send()
            sleep(self.frequency)
